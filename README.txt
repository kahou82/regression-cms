[Overview]

The PGA CMS application is a web application written with the Django framework to simplify and automate running regression tests. It allows for bookkeeping tasks like tracking bugs, maintaining pass/fail information, and organizing ownership/permissions.



[Data Model Overview]

User Authentication:
    - Users
    - Groups
        - Superuser (Django default)
        - Manager
        - Developer
Regression Specific:
    - Project
    - Test Info
    - Test Case
    - Run Record
    - Bug
    - Tag
    - RegressionGroup

Relationships:

User to Project is Many to Many.
Project to TestCase is One to Many.
Project to RegressionGroup is One to Many.
Project to TestInfo is Many to Many.
RegressionGroup to RunRecord is One to Many.
TestCase to Bug is Many to Many.
TestCase to RunRecord is One to Many.
Bug to Tag is Many to Many.



[User Levels]

- The superuser is a default user in the Django framework. It is similar to the Linux superuser in that it holds absolute power and all permissions.
- A manager is a user that has read/write/create/delete access too all projects. In addition, a manager can also edit and delete accounts.
- A developer is the base group every user is added to upon registration. A developer only has write access to projects in which he/she is listed as a contributor. All other projects are read only. Developers cannot create new projects and lack all delete access.



[Project]

A model that represents a project such as Warpath 10G and Wraith 40G.
Only managers and superusers can create or delete Projects.

Fields:
- Project Name: A string that describes the project.
- Project Collaborators: A list of developers that can edit and add to the project. All managers are automatically added to this field.
- Project Speed: The speed that the project will run on for regression testing. (FIBER10G, FIBER40G, LAN, FIBER100G)
- Test Sets: Various test sets that can be used to populate the list of test cases as opposed to manually entering in each test.
- Test Cases: A group of test cases that the project uses and runs.



[Test Case]

This model represents a test case, usually a .tcl file, that is used to do regression testing.

Fields:
- Test Name: A string representing the name of the test.
- Parent Project: The project that contains this test.
- Owner: The developer responsible for the test.
- Bugs: All bugs associated with the test that prevent it from always passing.
- Run Records: A list that contains all runs of the test.



[Test Info]

This model represents a group/set of test cases. These can be added as a ManyToMany field to Projects in order to pre-populate the Project with test cases. This is model is only used when creating a project. After the project is created, even though the project will have associated Test Infos, they will have no meaning when it comes to updating the Project model. Each test info corresponds to a hardware type, except "ALL", which of course, corresponds to loading all tests.

Fields:
- Name: The hardware type such as "BLINK" or "TBIRD".



[Bug]

The model that represents any CR or issue which prevents a test case from running successfully.

Fields:
- Bug Name: A string that represents the name of the bug.
- Bug Description: A string that gives more information about the bug.
- Bug ID: A number that is used to refer to the bug.
- Bug Status: A boolean to show whether or not a bug is resolved or not. Bugs are never deleted, only marked as resolved.
- Tags: Tags associated with the bug that allow for easier search and grouping.



[Tag]

A model that represents a tag on a bug used for easier identifying and searching. Essentially a simple string.

Fields: 
- Tag Name: The only field of the bug. A string that is the name of the tag.



[Run Record]

A model that represents a single run of a test. This is automatically created by the regression runner and placed in a RegressionGroup container.

Fields:
- Parent Test: The test that the run record is a record of.
- Record Group: The parent regression group that the records is a batch of.
- Record Owner: The person who ran the regression.
- Record Log: The pytest output log of the test.
- Record Passed: A boolean that is true if the test passed or false if it did not.



[Regression Group]

A model that represents a batch run of regression test. This model should only be created by the server.

Fields:
- Group Project: The project that the group was run on/associated with.
- Group Start: The start time of the regression test.
- Group End: The end time of the regression test.
- Group IL: The IL build that the test was run with.
- Group BLL: The BLL build that the test was run with.
- Group Runner: The person who executed the regression tests.
- Group Log: A log of the pytest output while running the test.
- Group File: A zip file of the detailed outputs created by pytest.



[Setting Up the Application]

Setting up the application is rather simple. It is recommended that you have the following packages (other versions may work, but the application was built using these versions): 

Python 2.7.3 - http://www.python.org/getit/
Django 1.5.1 - https://www.djangoproject.com/download/
Django South 0.8.1 -  http://south.readthedocs.org/en/latest/installation.html
django-admin-tools - https://bitbucket.org/izi/django-admin-tools/wiki/Home
MySQL 14.14 - http://www.mysql.com/downloads/ (OPTIONAL)

If using MySQL, you will need to set up a database with the name 'django_db' and give access to the user 'djangouser' with the password 'djangopassword'.

Once you have these dependencies, navigate to the pgacms directory through the console. Execute the command, "python init.py" and the server should be set up.



[Account Creation]

There will be a registration button on the main page of the admin login screen. Click that to register an account. After submitting the form, the account will be automatically created with Developer status. However, a developer only has write access to projects which he/she is explicitly listed as a collaborator. Otherwise, all pages regarding that project will be read-only. Contact the manager to be added to projects.



[Dashboard]

The first page a user sees is the dashboard, a page giving information about all projects the user is a collaborator on through dashboard modules. A new user will see nothing on this page at first, since he/she is not a collaborator on anything yet. Each dashboard module represents an individual project. Each module is movable, collapsable, and deletable. The module has this information:

Title: The title of the module is the name of the project being represented. Clicking on the title will bring the user to an information page about the project.

Total Tests: A link showing the number of tests in the project that leads to a list of all tests in the project.

Outstanding CRs: A link showing all unresolved CRs affecting the project that leads to a list of all unresolved CRs.

CRs in Last 7 Days: A link that leads to a list of all CRs created on the project in the last 7 days.

Total Regression Runs: A link showing the number of times the system has run regression tests on the project. The link leads to a list of each regression run, sorted by run date.

Tests By Owner: Shows a list of users and how many tests they own in the project.

Run Regression Tests: A small form that specifies 3 port pairs to run regression tests on the project. If less than 3 pairs are filled in, the regression will run on less than 3 pairs. Click the run regression button for the server to start regression tests.

The dashboard also contains a "Regression Graphs" module where the user can see graphical information about the projects. The two options are to look at CR History and to compare tests. CR History allows the user to select one project and see how many CRs were added over a time period. Compare tests allows the user to select 2+ tests and compare percentage of passing test scripts.

The dashboard also contains a "TestCenter Info" module where the user can see the current build of TestCenter being used by the server, the date it was uploaded, and the user who uploaded it. From this module, a user can also upload a version of TestCenter to be used by the server. See the corresponding Use Case for details.



[Use Case: Creating a Project]

Goal: Create a project as a manager and add developers to the project.

- Verify that your account is a manager or a superuser.
- On the regression tab of the top menu, click Project. This will take you to a page listing all of the current projects in the application.
- On the top right, click 'Add project'. This will take you to a new page which is a form to add a new project.
- Choose a project name.
- Specify a speed for the project. This should match the speeds used in the *_info.xml files. Some examples are FIBER10G, FIBER100G, FIBER40G, and LAN.
- Add various collaborators by double clicking on their names. Keep in minds that all managers are added by default to each project.
- Add test sets to populate the tests for the project. Adding the "ALL" set will load all tests.
- Add additional test cases if required. (Usually not.)
- Press Save on the bottom right and the server will automatically populate the test cases field with all test sets selected using tests in the regression Perforce folder pertaining to PGA, Capture, (Ignores repeats.)Learning, and RFC Benchmarking.
- To delete some of these unnecessary tests or add more users to collaborators, click on Regression>Project on the menu again. Instead of creating a new project on the resulting page, click on the project you wish to edit on the list of projects in the main body of the page.
- This will bring up a similar form where you can change the fields.



[Use Case: Seeing the Status of a TestCase]

Goal: Check to see if a certain test is passing or failing. If it is failing, check to see what Bugs/CRs are causing it to fail.

- Navigate to the dashboard.
- On the dashboard, find the module with the corresponding Project that contains the TestCase you are looking for.
- If you know the owner, click the proper link under "Tests by Owner". If not, click on the Total Tests link.
- Both of these links should take you to a page that shows a list of tests. Either manually look through these listings to find the test you are looking for, or use the sidebar to further filter or use the search box above the list.
- Once you find the test you can determine whether or not the test is passing or failing. If the test name is in green, then it is passing. If it is in red, then it is failing.
- Clicking on this colored name will bring you to an edit/info page of the test. Here you can see which bugs are affecting the test and the records from when the test was run.
- You can click the view link in the inline listing to further navigate to the bug in question.
- Alternatively, if you want to view a test that you are not a collaborator on, you can go to Regression>Test Case on the top menu. This will bring you to a listing page similar to the two mentioned previously, but will contain every test in the system regardless of project. (There might be duplicate names.)
- Use the search box and the right hand filter box to narrow down the list. Keep in mind that the tests you find this way that belong to Projects you are not a part of will be read-only.



[Use Case: Adding a Bug]

Goal: Add a bug after a test fails.

- Navigate to the CR/Bug list view. This can be done by going to Regression>Bug on the top menu bar. On this page, you can see a list of all bugs and filter them on the right by resolved/affected project.
- Make sure your bug does not already exist. This can usually be done by using the search bar. The search looks through CR Number, description, and Tags.
- If it does exist, remember the CR number.
- If it does not exist, click 'Add Bug' on the top right.
- On the resulting page, enter the Bug Name (usually the CR number), a description about what is wrong with the bug, and various tags describing the bug such as "CRC Error".
- Take care to ensure that you do not create duplicate tags by using existing tags from the drop down list.
- Press save.
- Navigate to the test you wish to add a bug to through any means. This is covered in more detail in the previous "Seeing the Status of a TestCase" use case.
- Scroll down to the bugs section and click "Add another Bug".
- Choose the existing bug or the bug you just added from the drop down.
- Press Save on the bottom rig
ht.



[Use Case: Running Regression Tests]

Goal: Run a project set of regression tests and have the server automatically update the database.

- Navigate back to the dashboard. On each module, you will see a section called Run Regression Tests with three port pairs.
- Enter up to three port pairs in the specified format of ip/slot/port. If a port pair is left blank, it will not be used while running the regression test.
- Click Run Regression, and you will be taken to a new screen with a list of all regression tests in the project.
- Select any number of tests to run. By default, all tests are selected. You can use the Check/Uncheck All checkbox to check and uncheck all items in that specific group such as PGA_BLL or CAPTURE_BLL.
- The tests are currently running and the page will update as new information is passed.
- Wait for the tests to finish and click Return to go back to the dashboard. Keep in mind that while this test is running, the server will prevent tests from all other projects from running.



[Use Case: Viewing Last Regression]

Goal: After running a regression, view the results of the test.

- To view the results of a test, we must navigate to the proper RegressionGroup view page. - This can be done easily through two methods.
- The first way is simply to go back to the dashboard and click the most recent run under �Latest Three Runs�.
- The second way is to click on �Total Regression Runs� in the dashboard module. The resulting page gives a list of all RegressionGroups, but is automatically sorted by date. Click the one on the top.
- Both of these methods will show you the pass/fail fraction.
- On the resulting page, we can download the regression group log zip file, view the standard output of the test, and see which tests passed or failed.
- After running a test, it is recommended to go through the failed tests and add corresponding bugs.



[Use Case: Update the Server's TestCenter Build]

Goal: Update the server's TestCenter build to a different version to run tests on.

- First, get the Linux build of the TestCenter version you want to run.
- To do this, open up the directory that it is in, and zip the entire directory. Keep in mind when zipping to not compress the parent directory. For example, if my build is in "Linux_EL3", I wouldn't right click on the Linux_EL3 folder and zip. Instead, I would open up Linux_EL3 and select every folder and file inside and zip that instead.
- Navigate to the dashboard, and locate the TestCenter Info module.
- Press choose file and select the file you just zipped.
- Press upload. The upload process takes some time. Please wait around 5-10 minutes.