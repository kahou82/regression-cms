package require SpirentTestCenter

foreach arg $argv {
  set portHnd [stc::create port -under project1 -location "$arg"]

  stc::perform attachports -portlist $portHnd -autoconnect true

  set hnd [stc::get $portHnd -physicallogical-sources]

  set hnd [stc::get $hnd -parent]
  set hnd [stc::get $hnd -parent]

  set ver [stc::get $hnd -firmwareversion]
  puts $ver
}
