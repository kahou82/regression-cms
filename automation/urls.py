from django.conf.urls import patterns, url

from automation import views

urlpatterns = patterns('',
    url(r'crajax/', views.CRView.as_view(), name="crajax"),
    url(r'testsajax/', views.TestsView.as_view(), name="testsajax"),
    url(r'^(?P<project>.+)', views.index, name='index'),
)