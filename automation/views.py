# This file concerns itself with the view for the regression run output page. The page updates through
# automated refreshing instead of AJAX and displays the same output as the Python console except with
# pretty colors in HTML.
# @author Brian Ho
# @date 7/22/13
# @email bho6@jhu.edu

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.utils import timezone
from django.core.files import File
from django.conf import settings
from django.views.generic.base import View, TemplateView
from regression.models import Project, TestCase, RunRecord, Bug, RegressionInfo, RegressionGroup
from upload.models import TCInfo
from subprocess import call, Popen, PIPE
from datetime import datetime, date, timedelta
from collections import defaultdict
from regression.admin import generate_entry
import threading
import os
import zipfile
import json

# A global variable that stores a list of lines from the output of pytest. It is global so the application
# can easily fetch information from the worker/runner thread.
log_output = []

# A global variable that shows whether the regression test is running or not. This is used in the dashboard
# to know when to disable Run Regression buttons.
running = False

# A global variable that stores the name of the running regression. This is used in the dashboard to know
# which test is running so the application can add a 'View Running Regression' button.
runningproj = ''

# A global variable that stores the name of the user running the regression. This is used in the dashboard so
# others can see who is running the regression.
runninguser = ''

# A helper method used to reduce cyclomatic complexity that parses lines and sees if the tests are passing
# or failing. Automatically add RunRecords to the RegressionGroup and TestCases. Returns a dictionary that
# contains information passed to the template through the context.
# @param proj: the project being used to run regression
# @param user: the user that requested the regression
# @param group: the group the project should be added to
# @param out: the line to be processed
# @return: a dictonary with information such as color and text that will be added to the context variable
def parse_input(proj, user, group, out):
    dict = {}
    if out.find('PASSED') == 6:
        dict = {'str': out, 'info': 'green'}
        start = out.rfind('test: ') + 6
        end = out.rfind('_' + proj.xml_str())
        rr = RunRecord(parent_test = TestCase.objects.filter(parent_project=proj, test_name=out[start:end])[0],
                       record_owner = user,
                       record_log = out,
                       record_group = group,
                       record_passed = True)
        rr.save()
    elif out.find('FAILED') == 6:
        dict = {'str': out, 'info': 'red'}
        start = out.rfind('test: ') + 6
        end = out.rfind('_' + proj.xml_str())
        rr = RunRecord(parent_test = TestCase.objects.filter(parent_project=proj, test_name=out[start:end])[0],
                       record_owner = user,
                       record_log = out,
                       record_group = group,
                       record_passed = False)
        rr.save()
    else:
        dict = {'str': out, 'info': 'black'}
    return dict

# A worker class that is used as a thread to concurrently run the application logic and the regression tests
# at the same time. This allows for asynchronous updating of the regression output page without stalling
# the rest of the web pages. This uses Popen and runs through the python shell on the list of ports specified
# in the RegressionInfo of the Project. If ports are left blank, they will be ignored, making it possible to
# run on only 1 or 2 port pairs if needed. This method also counts passed and failed and generates a
# RegressionGroup containing all of the RunRecords created. It also zips the resulting output folder from
# pytest and uploads it as an attachment to RunRecords.
# @param proj: the project that we are running regressions on
# @param user: the user that clicked 'Run Regressions' (will also be the owner of all resulting run records)
def runner(proj, user, bypass):
    global running
    global runningproj
    global log_output
    log_output = []
    if bypass == True:
        log_output.append({'str': 'Starting tests...', 'info': 'black'})
    else:
        log_output.append({'str': 'Validating ports...', 'info': 'black'})
    pcount = 0
    ports = ''
    
    # Gets the ports being used. Stop regression test if no ports pairs have been entered.
    portsl = port_list(proj)
    usedports = []
    for num in range(0, len(portsl), 2):
        if portsl[num] != '' and portsl[num + 1]:
            pcount += 1
            ports += '(\"//%s\", \"//%s\"),' % (portsl[num], portsl[num + 1])
            usedports.append(portsl[num])
            usedports.append(portsl[num + 1])
    if pcount != 0 and ports[len(ports)-1] == ',':
        ports = ports[:-1]
    elif pcount == 0:
        log_output = []
        log_output.append({'str': 'No ports selected.', 'info': 'black'})
        log_output.append({'str': 'Return', 'info': 'complete'})
        runningproj = ''
        running = False
        return
    
    if bypass == False:
        # Run through each port with the firmware version script to get the firmware version running on each port.
        # Add the firmware versions to a set and add a dictionary entry for the port and version. If the .tcl
        # script is unable to locate the port, return an error and stop the regression test.
        ildict = []
        ilset = set()
        commands = ['tclsh', 'ilversion.tcl']
        commands.extend(usedports)
        proc = Popen(commands, stdout=PIPE, stderr=PIPE, cwd=os.path.dirname(os.path.realpath(__file__)))
        err = proc.stderr.read()
        if err.find('One or more ports could not be brought online.') == -1 and err.find("can't find package SpirentTestCenter") == -1:
            output =  proc.stdout.read().split('\n')
            output.pop()
            for i in range(0, len(usedports)):
                ildict.append({'port': usedports[i], 'version': output[i]})
                ilset.add(output[i])
        else:
            log_output = []
            log_output.append({'str': 'Invalid ports.', 'info': 'black'})
            for line in err.split('\n'):
                log_output.append({'str': line, 'info': 'black'})
            log_output.append({'str': 'Return', 'info': 'complete'})
            runningproj = ''
            running = False
            return

        # Since the set has no repeats, we check the size of the set to see if there is more than one IL version.
        # If len > 1, then stop the regression because of version mismatch and print a list of ports and versions.
        if len(ilset) != 1:
            log_output = []
            log_output.append({'str': 'Firmware version mismatch.', 'info': 'black'})
            log_output.append({'str': ' ', 'info': 'black'})
            log_output.append({'str': 'List of port versions:', 'info': 'black'})
            for port_v in ildict:
                log_output.append({'str': port_v['port'] + ': ' + port_v['version'], 'info': 'black'})
            log_output.append({'str': 'Return', 'info': 'complete'})
            runningproj = ''
            running = False
            return

    # Run the test through pytest.
    log_output = []
    commands = ['python', settings.REG_PATH + "/run.py", 
                "--hw='{\"%s\":[%s]}'" % (proj.xml_str(), ports), 
                "-n " + str(pcount),
                "-m '(PGA_BLL or CAPTURE_BLL or LEARNING_BLL or RFC_BENCHMARKING_BLL) and HW_%s and not slow and not disabled'" % (proj.xml_str()),
                "--archive-tmpdir",
                "--timeout=600",
                "--keep-all-tmpdir",
                "--stc-pkg-dir=" + settings.STC_INSTALL_DIR,
                "--basetemp=" + settings.LOG_PATH]

    output = Popen(commands, stdout=PIPE)
    bll = 'None'
    il = 'Bypassed'
    if bypass == False:
        il = ilset.pop()
    tciset = TCInfo.objects.all()
    if tciset.exists():
        bll = tciset[0].tcinfo_bll
    group = RegressionGroup(reg_group_project=proj, 
                            reg_group_start = timezone.now(),
                            reg_group_end = timezone.now(),
                            reg_group_bll = bll,
                            reg_group_il = il,
                            reg_group_runner = user)
    group.save()
    wholelog = ''
    # Read each new line until pytest is done.
    while output.poll() is None:
        out = output.stdout.readline()
        if out != '':
            log_output.append(parse_input(proj, user, group, out))
            wholelog += out
    # We must go through the remaining lines in stdout because there is the chance that there could still be
    # lines in between our last processed line and the end of process.
    for ln in output.stdout:
        if ln != '':
            log_output.append(parse_input(proj, user, group, ln))
            wholelog += ln
    group.reg_group_end = timezone.now()
    group.reg_group_log = wholelog
    # Zip the pytest output folder.
    zf = zipfile.ZipFile(settings.ZIP_PATH + '/' + group.file_name() + '.zip', "w")
    for dirname, subdirs, files in os.walk(settings.LOG_PATH):
        zf.write(dirname)
        for filename in files:
            zf.write(os.path.join(dirname, filename))
    zf.close()
    with open(settings.ZIP_PATH + '/' + group.file_name() + '.zip', 'rb') as zip_file:
        group.reg_group_file.save(settings.ZIP_PATH + '/' + group.file_name() + '.zip', File(zip_file), save=True)
    group.save()
    log_output.append({'str': 'Return', 'info': 'complete'})
    runningproj = ''
    running = False

# A method that creates the XML files in the REG_PATH directory. This method first deletes all existing
# XML files pertaining to that project by exploring all subfolders for filenames and then creates XML
# files in the proper directories using generate_entry(). This differs from the create_xml in admin.py because
# instead of adding all tests in a project, it only adds tests in a second argument called testlist.
# @param proj: the project that is being changed
# @param testlist: a list of TestCase objects that will be used to create the .xml files.
def create_xml(proj, testlist):
    data = defaultdict(list)
    for root, dirs, files in os.walk(settings.REG_PATH):
        for file in files:
            if str(file) == proj.xml_str() + '_info.xml':
                p = os.path.join(root, file)
                os.remove(os.path.abspath(p))
    for test in testlist:
        data[test.test_folder].append(generate_entry(test.test_name, test.test_group, test.parent_project.xml_str(), proj.project_speed))
    for key, value in data.iteritems():
        xmlstring = '<dir>\n'
        for entry in data[key]:
            xmlstring += entry
        xmlstring += '</dir>'
        with open('%s/%s/%s_info.xml' % (settings.REG_PATH, key, proj.xml_str()), 'w') as sfile:
            sfile.write(xmlstring)

# A method that takes all of the ports from the regression info data model and puts them into a list for easy
# iteration. Used to save time instaed of repeating the same code block 10 times.
# @param oroj: the project to get the ports from.
# @return the list of the ports being used.
def port_list(proj):
    p = proj.project_regression_info
    plist = [p.regression_portone, p.regression_porttwo, p.regression_portthree, p.regression_portfour,
             p.regression_portfive, p.regression_portsix, p.regression_portseven, p.regression_porteight,
             p.regression_portnine, p.regression_portten]
    return plist

# A method that creates handles the view object for the regression output display page. It starts a thread
# of the runner class to carry out the regression. The template contains logic where if 'running' in the
# context is set to True, the page will refresh every 3 seconds to update the output. When 'running' is
# False, then the refreshing will stop and a 'Return' button will be added to the page. The main content of
# the page will be passed through 'reg_log' with log_output.
# @param request: the reqest sent to the page which contains POST/GET data
# @param project: the project that we are running regression on
def index(request, project):
    global running
    projects = Project.objects.filter(project_name = project)
    if running == False and projects.exists() and request.method == "POST":
        proj = projects[0]
        testlist = []
        for test in request.POST.getlist('test'):
            testlist.append(TestCase.objects.filter(parent_project=proj, test_name=test)[0])
            
        create_xml(proj, testlist)
        bypass = False
        if 'bypass' in request.POST:
            if request.POST['bypass'] == 'True':
                bypass = True
        t = threading.Thread(target=runner, args=(Project.objects.filter(project_name=project)[0], request.user, bypass))
        t.start()
        global runningproj
        global runninguser
        running = True
        runningproj = str(project)
        runninguser = str(request.user)
        if bypass == True:
            context = {'reg_log': [{'str': 'Starting tests...', 'info': 'black'}], 'running': running}
        else:
            context = {'reg_log': [{'str': 'Validating ports...', 'info': 'black'}], 'running': running}
    else:
        context = {'reg_log': log_output, 'running': running}
    return render(request, 'automation/index.html', context)

# A Class based view that takes an AJAX request from chart_module.html. This class is used for the "Compare
# Tests". In the request, we get the form data from the chart_module.html template. All of the validation
# regarding how many selected tests there are is taken care of client side through javascript. This class
# creates a graph using Google Charts and returns the image file back to the template for rendering.
class TestsView(View):
    
    # A method that processes the form info. It takes a list of projects selected on the template and adds
    # all of the project names to the X axis labels string. We then iterate through all of the projects to be
    # compared and count the passing tests they have. We we then use the total number of tests to calculate
    # the percentage of passing tests which we then feed into a graph values string. Finally, we put this info
    # all together to create a url for the Google Charts API and return that back to the template.
    # @param request: the request object from the template that contains data about the form info.
    # @return the HTTPResponse in json form passed back to the template.
    def get(self, request):
        data = {}
        listproj = request.GET["data"].split(',')
        values = ''
        projects = ''
        for item in Project.objects.all():
            if str(item) in listproj:
                projects += '|' + str(item)
                failed = 0
                for test in TestCase.objects.filter(parent_project=item):
                    if (Bug.objects.filter(test_parent=test, bug_status=False).count() > 0):
                        failed += 1
                values += str(100-((failed*100)/TestCase.objects.filter(parent_project=item).count())) + ','
        chart = ('chart.googleapis.com/chart' +\
           '?chxl=1:%s' +\
           '&chxr=0,0,105' +\
           '&chxt=y,x' +\
           '&chbh=r,8,2' +\
           '&chs=500x225' +\
           '&cht=bvg' +\
           '&chco=CCCCFF' +\
           '&chds=0,105' +\
           '&chtt=Percent of Scripts Passing' +\
           '&chts=676767,11' +\
           '&chd=t:%s') % (projects, values[:-1])
        data = {'url': chart.replace(" ", "%20")}
        data = json.dumps(data)
        return HttpResponse(data, mimetype='json')

# A Class based view that takes an AJAX request from chart_module.html. This class is used for the "CR
# History". In the request, we get the form data from the chart_module.html template. All of the validation
# regarding how many selected tests there are is taken care of client side through javascript. This class
# creates a graph using Google Charts and returns the image file back to the template for rendering.
class CRView(View):
    def time_format(self, dt):
        return dt.strftime("%m/%d")
    
    # Similar to the get function in TestsView(), but more involved. This method takes the form data from the
    # template which in this case, is always a single project. It then initializes an int dictionary with keys
    # corresponding to the last 7 days. The method uses a set to get all of the bugs associated with TestCases
    # in the project being analyzed. Next, all of the bugs are iterated through. If they were created in the
    # last 7 days, then we add them to the corresponding key-value pair in the dictionary. We then construct
    # a graph similar to in TestsView() with the information in this dictionary and return it back to the
    # template.
    # @param request: the request object from the template that contains data about the form info.
    # @return the HTTPResponse in json form passed back to the template.
    def get(self, request):
        data = {}
        listproj = request.GET["data"]
        daysnum = int(float(request.GET["days"]))
        if daysnum > 100:
            daysnum = 100
        values = ''
        names = ''
        yaxis = ''
        proj = Project.objects.filter(project_name=listproj)[0]
        bugs = set()
        daycount = {}
        for day in range(0, daysnum + 1):
            daycount[self.time_format(datetime.today() - timedelta(days=day))] = 0
        for test in TestCase.objects.filter(parent_project=proj):
            for bug in test.test_bugs.all():
                bugs.add(bug)
        for bug in bugs:
            if (datetime.now() - bug.create_date.replace(tzinfo=None)).days < daysnum:
                daycount[self.time_format(bug.create_date)] += 1
        
        max = 1
        for day in range(daysnum - 1, -1, -1):
            localday = self.time_format(datetime.today() - timedelta(days=day))
            names += '|' + localday
            values += str(daycount[localday]) + ','
            if daycount[localday] > max:
                max = daycount[localday]
        
        # If the max number of bugs on a certain day is less than 10, we need to add labels manually in order
        # to override the default labeling which will add decimal values to the y-axis. (ex: 0.2, 0.4, 0.6).
        # If max is over 10, we increase max by 20% to add some breathing room near the top of the graph.
        if max < 10:
            yaxis = '|0:'
            for x in range(0, max + 1):
                yaxis += '|'  + str(x)
        else:
            max *= 1.2
        chart = ('chart.googleapis.com/chart' +\
           '?chxl=1:%s%s' +\
           '&chxr=0,0,%s' +\
           '&chxt=y,x' +\
           '&chbh=r,8,2' +\
           '&chs=500x225' +\
           '&cht=bvg' +\
           '&chco=CCCCFF' +\
           '&chds=0,%s' +\
           '&chtt=Bugs Created by Date: %s' +\
           '&chts=676767,11' +\
           '&chd=t:%s') % (names, yaxis, max, max, listproj ,values[:-1])
        data = {'url': chart.replace(" ", "%20"), 'proj': listproj}
        data = json.dumps(data)
        return HttpResponse(data, mimetype='json')




# Class based view that is shown when 'Run Regression Tests' is clicked on. This takes users to a list of
# checkboxes corresponding to every test in the project. Users can select any number of tests to run
# regressions on and click the button at the bottom of the page to run the selected tests.
class SelectView(TemplateView):
    template_name = "automation/select.html"

    # A method that takes post data and renders the checkbox form list of tests. In addition to this base
    # behavior, it also saves the regression_info ports. If the page was reached through invalid means, it
    # will redirect back to the admin homepage. This passes the bypass information to the actual regression
    # running page through a hidden input on the template.
    # @param request: the request object from the template that contains data about the form info.
    # @param *args: contains positional arguments and is not used.
    # @param **kwargs: contains arguments passed through the URL such as 'project'.
    # @return the proper template with the context with all of the tests.
    def post(self, request, *args, **kwargs):
        projects = Project.objects.filter(project_name = kwargs['project'])
        if 'one' in request.POST and 'two' in request.POST and 'three' in request.POST \
            and 'four' in request.POST and 'five' in request.POST and 'six' in request.POST and projects.exists():
            proj = projects[0]
            proj.project_regression_info.regression_portone = request.POST['one']
            proj.project_regression_info.regression_porttwo = request.POST['two']
            proj.project_regression_info.regression_portthree = request.POST['three']
            proj.project_regression_info.regression_portfour = request.POST['four']
            proj.project_regression_info.regression_portfive = request.POST['five']
            proj.project_regression_info.regression_portsix = request.POST['six']
            proj.project_regression_info.regression_portseven = request.POST['seven']
            proj.project_regression_info.regression_porteight = request.POST['eight']
            proj.project_regression_info.regression_portnine = request.POST['nine']
            proj.project_regression_info.regression_portten = request.POST['ten']
            proj.project_regression_info.save()
            proj.save()
            bypass = False
            if 'bypass' in request.POST:
                bypass = True
            testdict = defaultdict(list)
            for test in TestCase.objects.filter(parent_project = proj):
                testdict[test.test_group].append(str(test))
            context = {'tests': dict(testdict), 'url': proj, 'bypass': bypass}
            return self.render_to_response(context)
        else:
            return HttpResponseRedirect('/admin/')
        
    # A method that redirects back to the admin dashboard if the page does not have POST data.
    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect('/admin/')