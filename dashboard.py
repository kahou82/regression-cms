# This file creates an intuitive and informative dashboard module by extending the DashboardModule in django
# admin tools. This provides some base functionality such as collapsable and deletable modules.
# @author Brian Ho
# @date 7/22/13
# @email bho6@jhu.edu

from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.core.urlresolvers import reverse
from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard
from admin_tools.utils import get_admin_site_name
from django.utils.html import format_html
from regression.models import Project, TestCase, Bug, RegressionGroup
from upload.models import TCInfo
from collections import defaultdict
from datetime import datetime
from datetime import timedelta
from automation import views
from upload import forms, models

# This class defines the secondary dashboard module that can show two types of graphs for projects. The first
# type of graph is a CR graph. This is a graph that takes one project and shows a bar graph of number of CRs
# created on the project in the last 7 days. The second type of graph is a comparison graph, where the user
# can select 2+ projects and see a bar graph comparison based on percentage of passing tests.
class ChartModule(modules.DashboardModule):
    template = 'chart_module.html'
    
    # A method inherited from modules.DashboardModule that is called when it is displayed on the admin 
    # homepage. This uses the chart_module.html template that utilizes AJAX to create a smooth transition from
    # the selection project page to the graph display page.
    # @param context: the context passed to the module by the application    
    def init_with_context(self, context):
        modules.DashboardModule.__init__(self, 'Regression Graph')
        pnames = []
        for proj in Project.objects.all():
            pnames.append(str(proj))
        self.children.append({'project_names': pnames})
        self.title = 'Regression Graphs'
        self.initialized = True
        

# This class defines the secondary dashboard module that allows the user to upload a version of TestCenter to
# be used for running regression tests. Uploads and subsequent processing will take around 5-10 minutes to
# complete. This automatically discovers the BLL version being used and updates the database with this BLL
# version. This version will be displayed on all regression record logs. Adds a hidden input field to the
# template form to pass the user data.
class UploadModule(modules.DashboardModule):
    template = 'upload/upload_module.html'
    
    # A method inherited from modules.DashboardModule that is called when it is displayed on the admin 
    # homepage. This uses upload_module.html and upload/forms.py to create an interface for displaying and
    # uploading TestCenter.
    # @param context: the context passed to the module by the application    
    def init_with_context(self, context):
        modules.DashboardModule.__init__(self, 'TestCenter Info')
        f = forms.UploadForm
        build = 'None'
        date = 'None'
        uploader = 'None'
        tciset = TCInfo.objects.all()
        if tciset.exists():
            build = tciset[0].tcinfo_bll
            date = timezone.localtime(tciset[0].create_date).strftime("%Y/%m/%d %I:%M:%S%p")
            uploader = tciset[0].tcinfo_uploader
        dict = {'form': f,
                'build': build,
                'date': date,
                'uploader': uploader,
                'username': str(context['request'].user)
                }
        self.children.append(dict)
        self.title = 'TestCenter Info'
        self.initialized = True
        

# This class defines the main dashboard modules. Each module represents a project that contains info regarding
# total tests, outstanding CRs, recent CRs, regression runs, latest regression runs, tests by owner, and a
# small console for running regression tests.
class ProjectModule(modules.DashboardModule):
    template = 'project_list.html'
    
    # This method allows the class to be assigned a project upon initialization.
    # @param i: the project the module represents
    def __init__(self, i):
        self.item = i
    
    # A method inherited from modules.DashboardModule that is called when it is displayed on the admin 
    # homepage.
    # @param context: the context passed to the module by the application    
    def init_with_context(self, context):
        modules.DashboardModule.__init__(self, str(self.item))
        count = TestCase.objects.filter(parent_project=self.item).count()
        info = {}
        testinfo = {}
        failed = 0
        total = 0;
        s = set()
        tests = defaultdict(int)
        # Get bugs.
        for test in TestCase.objects.filter(parent_project=self.item):
            check = False
            total += test.test_bugs.count()
            tests[test.owner] += 1
            for bug in test.test_bugs.all():
                if bug.bug_status == False:
                    check = True
                    s.add(bug)
            if check == True:
                failed += 1
        # Add links and info for the 'Project Overview' setion of the dashboard modules.
        testinfo = {'passing': (count - failed), 
                    'total': count,
                    'bugs': len(s),
                    'testurl': 'regression/testcase/?parent_project__id__exact=' + str(self.item.id),
                    'bugsurl' : 'regression/bug/?bug_status__exact=0&project=' + str(self.item),
                    'weeklybugsurl' : 'regression/bug/?create_date__gte=' + \
                    str((datetime.today() + timedelta(-7)).strftime("%Y-%m-%d")) + \
                    '+00%3A00%3A00-07%3A00&create_date__lt=' + \
                    str((datetime.today() + timedelta(+1)).strftime("%Y-%m-%d")) + \
                    '+00%3A00%3A00-07%3A00&project=' + str(self.item)}
        # Get tests by owner.
        tlist = {}
        for test in tests:
            tlist[str(test)] = {
                    'title': str(test),
                    'url': 'regression/testcase/?owner__id__exact=' + str(test.id) + '&parent_project__id__exact=' + str(self.item.id),
                    'owned': str(tests[test])
                }
        # Get the regression pairs from project_regression_info.
        pinfo = {
                    'name': self.item.project_name,
                    'one': self.item.project_regression_info.regression_portone,
                    'two': self.item.project_regression_info.regression_porttwo,
                    'three': self.item.project_regression_info.regression_portthree,
                    'four': self.item.project_regression_info.regression_portfour,
                    'five': self.item.project_regression_info.regression_portfive,
                    'six': self.item.project_regression_info.regression_portsix,
                    'seven': self.item.project_regression_info.regression_portseven,
                    'eight': self.item.project_regression_info.regression_porteight,
                    'nine': self.item.project_regression_info.regression_portnine,
                    'ten': self.item.project_regression_info.regression_portten
                }
        # Add a link for total regression runs.
        records = RegressionGroup.objects.filter(reg_group_project=self.item).order_by('reg_group_start')
        urls = []
        # Get last three regression runs.
        for x in range(0, 3):
            if x < records.count():
                index = records.count() - x - 1
                urls.append({'url': 'regression/regressiongroup/' + str(records[index].id),
                             'time': str(records[index]),
                             'pass': records[index].get_passed()})
        rinfo = {
                  'projurl': 'regression/regressiongroup/?reg_group_project__id__exact=' + str(self.item.id),
                  'count': records.count(),
                  'latest': urls
                }
        # Append everything to a context and pass it to the template.
        info = {
                'testinfo': testinfo,
                'testlist': tlist,
                'projectinfo': pinfo,
                'reggroupinfo': rinfo,
                'rinfo': {'running': views.running, 'project': views.runningproj == str(self.item), 'user': views.runninguser}
                }
        self.children.append(info)
        self.title = str(self.item)
        self.title_url = 'regression/project/%d' % self.item.id
        self.initialized = True

# Override for the default dashboard. This populates the dashboard with modules that use the custom 
# ProjectModule class. Sets the default number of columns on the dashboard to 2.
class CustomIndexDashboard(Dashboard):
    columns = 2
    
    # Called when the index page is loaded for the django admin. This populates the dashboard with modules
    # corresponding to each project that the user is a collaborator on.
    # @param context: the context passed to the module by the application      
    def init_with_context(self, context):
        site_name = get_admin_site_name(context)
        for item in Project.objects.all():
            if item.project_collaborators.all().filter(username=context["request"].user).exists():
                self.children.append(ProjectModule(item))
        self.children.append(ChartModule())
        self.children.append(UploadModule())
        
#         graph_list = get_active_graph()
#         for i in graph_list:
#             kwargs = {}
#             #kwargs['chart_size'] = "380x100" # uncomment this option to apply your graph size
#             kwargs['graph_key'] = i.graph_key
# 
#             self.children.append(DashboardChart(**kwargs))

# Currently unused override. Might remove in the future.
class CustomAppIndexDashboard(AppIndexDashboard):
    title = ''
    def __init__(self, *args, **kwargs):
        AppIndexDashboard.__init__(self, *args, **kwargs)
        self.children += [
            modules.ModelList(self.app_title, self.models),
            modules.RecentActions(
                _('Recent Actions'),
                include_list=self.get_app_content_types(),
                limit=5
            )
        ]
        
    def init_with_context(self, context):
        return super(CustomAppIndexDashboard, self).init_with_context(context)
