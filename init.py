from django.conf import settings
from django.core.management import call_command
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "pgacms.settings")
from django.contrib.auth.models import Group, Permission, User
from regression.models import TestInfo

 
call_command('syncdb', interactive=True)
call_command('migrate')

print 'Adding user groups...'

managers = Group(name='Manager')
managers.save()
perms = ['Can add group',
         'Can change group',
         'Can delete group',
         'Can add permission',
         'Can change permission',
         'Can delete permission',
         'Can add user',
         'Can change user',
         'Can delete user',
         'Can add dashboard preferences',
         'Can change dashboard preferences',
         'Can delete dashboard preferences',
         'Can add bug',
         'Can change bug',
         'Can delete bug',
         'Can add project',
         'Can change project',
         'Can delete project',
         'Can add regression group',
         'Can change regression group',
         'Can delete regression group',
         'Can add regression info',
         'Can change regression info',
         'Can delete regression info',
         'Can add run record',
         'Can change run record',
         'Can delete run record',
         'Can add tag',
         'Can change tag',
         'Can delete tag',
         'Can add test case',
         'Can change test case',
         'Can delete test case',
         'Can add test info',
         'Can change test info',
         'Can delete test info',
         'Can change revision',
         'Can change version',
         'Can add tc info',
         'Can change tc info',
         'Can delete tc info']
for perm in perms:
    managers.permissions.add(Permission.objects.filter(name=perm)[0])
managers.save()


developers = Group(name='Developer')
developers.save()
perms = ['Can add dashboard preferences',
         'Can change dashboard preferences',
         'Can delete dashboard preferences',
         'Can add bug',
         'Can change bug',
         'Can change project',
         'Can add regression group',
         'Can change regression group',
         'Can add regression info',
         'Can change regression info',
         'Can add run record',
         'Can change run record',
         'Can add tag',
         'Can change tag',
         'Can add test case',
         'Can change test case',
         'Can add test info',
         'Can change test info',
         'Can change revision',
         'Can change version',
         'Can add tc info',
         'Can change tc info']
for perm in perms:
    developers.permissions.add(Permission.objects.filter(name=perm)[0])
developers.save()

for user in User.objects.all():
    if user.is_superuser == True:
        user.groups = [managers, developers]
    else:
        user.groups = [developers]

print 'Adding default test sets...'

sets = ['ALL',
        'WRAITH10G',
        'WRAITH40G',
        'WARPATH10G',
        'WARPATH40G',
        'TBIRD',
        'TBONAONIC',
        'FRENZY',
        'BLINK',
        'QEMU']

for name in sets:
    ts = TestInfo(info_name = name)
    ts.save()
    
print 'Done!'