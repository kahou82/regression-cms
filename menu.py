# This file overrides the default django admin tools menubar along the top of each page to create a more
# intuitive menu bar that distinguishes between Developers and Managers and gets rid of unnecessary info.
# @author Brian Ho
# @date 7/22/13
# @email bho6@jhu.edu

from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.db.models import get_app, get_models
from regression.models import Project
from admin_tools.menu import items, Menu
from admin_tools.menu.items import MenuItem

# This class defines a custom menu for the PGA CMS application. This menu distinguishes between Managers and
# Developers. Managers will see a separate drop down pertaining to account administration. This also overrides
# the default functionality of the drop down list with intermediate drop down lists into just one drop down 
# list.
class CustomMenu(Menu):
    
    # This initializes the menu with its parent initialization.
    # @param **kwargs: parameters passed that do not really matter for our usage.
    def __init__(self, **kwargs):
        Menu.__init__(self, **kwargs)

    # Called when the menu is loaded from any page. Gives Managers some more information and options and
    # populates the menu with cutsom AppMenuItems.
    # @param context: context passed to the menu by the application contianing information like the user
    # who requested the page.
    def init_with_context(self, context):
        if context['request'].user.groups.all().filter(name='Manager').exists() or context['request'].user.is_superuser == True:
            self.children += [
                items.MenuItem(_('Dashboard'), reverse('admin:index')),
                AppMenuItem('regression', 'Regression'),
                AppMenuItem('auth', 'Administration')
            ]
        else:
            self.children += [
            items.MenuItem(_('Dashboard'), reverse('admin:index')),
            AppMenuItem('regression', 'Regression'),
            ]
        return super(CustomMenu, self).init_with_context(context)

# This class creates a custom menu item. Defines various custom behaviors to selectively show certain models
# and hiding other unimportant default models such as 'Permission'. This also hides models such as
# 'RegressionInfo' and 'TestInfo' which are important to the function of the application, but should not be
# changed through conventional django admin means.
class AppMenuItem(MenuItem):
    
    # Allows the menu item to be initialized with a name and a title.
    # @param name: the name of the application being requested (regression, auth)
    # @param title: the readable title on the menu
    def __init__(self, name, title, **kwargs):
        self.name = name
        self.title = title
        super(AppMenuItem, self).__init__(**kwargs)
        
    # Called when the menu item is loaded from the menu. Only show the TestInfo model if the user requesting
    # is a Manager. Never show the RegressionInfo model. Never show the Permission model.
    # @param context: context passed to the menu by the application contianing information like the user
    # who requested the page.
    def init_with_context(self, context):
        app = get_app(self.name)
        for model in get_models(app):
            if model._meta.object_name == 'TestInfo' and not (context['request'].user.groups.all().filter(name='Manager').exists() or context['request'].user.is_superuser == True):
                continue
            if model._meta.object_name == 'RegressionInfo':
                continue
            if model._meta.object_name != 'Permission':
                name ='/admin/%s/%s' % (self.name, model._meta.object_name.lower())
                self.children.append(MenuItem(title=model._meta.verbose_name.title(), url=name),)
        
