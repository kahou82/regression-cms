from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from pgacms.views import AdminRedirectView, TestFormView
from automation.views import SelectView

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'pgacms.views.home', name='home'),
    # url(r'^pgacms/', include('pgacms.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^$', AdminRedirectView.as_view()),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^register/', include('register.urls', namespace="register")),
    url(r'^automation/', include('automation.urls', namespace="automation")),
    url(r'^upload/', include('upload.urls', namespace="upload")),
    url(r'^select/(?P<project>.+)', SelectView.as_view(), name='select'),



    url(r'^testform/', TestFormView.as_view())
)+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()