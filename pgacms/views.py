from django.views.generic import View
from django.http import HttpResponseRedirect
from django.shortcuts import render

class AdminRedirectView(View):
    def get(self, form):
        return HttpResponseRedirect('/admin/')


class TestFormView(View):
	def get(self, request):
		return render(request, 'testform.html')
	def post(self, request):
		print request.FILES
		return render(request, 'testform.html')