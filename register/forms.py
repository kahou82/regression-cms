from django import forms
from django.contrib.auth.models import User, Group

class UserCreationForm(forms.ModelForm):
    error_messages = {
        'duplicate_username': "A user with that username already exists.",
        'password_mismatch': "The password fields didn't match.",
    }
    username = forms.RegexField(label="Username", min_length=3, max_length=30,
        regex=r'^[\w]+$',
        #help_text = "Required. 5 - 30 characters. Letters, digits only",
        error_messages = {
            'invalid': "This value may contain only letters, numbers only. The length of username is between 5 to 30 characters"})

    # use regex instead? r'^[\w]+[@][\w]+[.][\w]+$'
    email = forms.EmailField(label="Email")

    password1 = forms.CharField(label="Password",
           widget=forms.PasswordInput)
    
    password2 = forms.CharField(label="Password Confirmation",
           widget=forms.PasswordInput,
           #help_text = "Enter the same password as above, for verification.")
           )

    class Meta:
        model = User
        fields = ("username",)

    def clean_username(self):
        username = self.cleaned_data['username']
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1", "")
        password2 = self.cleaned_data["password2"]

        if password1 != password2:
            raise forms.ValidationError(self.error_messages['password_mismatch'])
        return password2

    def clean_email(self):
        email = self.cleaned_data["email"]
        email_count = User.objects.filter(email=email)[:1]

        if email_count:
             raise forms.ValidationError('Another account has been registered with this email address')
        else:
            return email

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.email = self.cleaned_data["email"]
        user.is_staff = True
        if commit:
            user.save()
            user.groups.add(Group.objects.get(name='Developer'))
        return user
