from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth import authenticate, login
from django.views.generic.edit import FormView
from .forms import UserCreationForm

class RegisterView(FormView):
    template_name = 'signup.html'
    form_class = UserCreationForm
    success_url ='/register'
    
    def form_valid(self, form):
        username = form.clean_username()
        password = form.clean_password2()
        form.save();
        user = authenticate(username=username, password=password)
        login(self.request, user)
        return redirect('/admin')
