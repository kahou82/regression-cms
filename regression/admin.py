# This file primarily concerns itself with displaying and configuring the various admin
# forms of the PGA CMS application. It builds on top of the django admin and django admin
# tools packages.
# @author Brian Ho
# @date 7/22/13
# @email bho6@jhu.edu

from django.contrib import admin
from regression.models import Project, TestCase, RunRecord, Bug, Tag, TestInfo, RegressionInfo, RegressionGroup
from django.db.models import Q
from django.contrib.auth.models import User
from django.contrib.admin import SimpleListFilter
from django.utils.safestring import mark_safe
from django.conf import settings
from subprocess import call
from subprocess import Popen, PIPE
from collections import defaultdict
import reversion
import re
import os 

# A method that creates the XML files in the REG_PATH directory. This method first deletes all existing
# XML files pertaining to that project by exploring all subfolders for filenames and then creates XML
# files in the proper directories using generate_entry().
# @param proj: the project that is being changed
def create_xml(proj):
    data = defaultdict(list)
    for root, dirs, files in os.walk(settings.REG_PATH):
        for file in files:
            if str(file) == proj.xml_str() + '_info.xml':
                p = os.path.join(root, file)
                os.remove(os.path.abspath(p))
    for test in TestCase.objects.filter(parent_project=proj):
        data[test.test_folder].append(generate_entry(test.test_name, test.test_group, test.parent_project.xml_str(), proj.project_speed))
    for key, value in data.iteritems():
        xmlstring = '<dir>\n'
        for entry in data[key]:
            xmlstring += entry
        xmlstring += '</dir>'
        with open('%s/%s/%s_info.xml' % (settings.REG_PATH, key, proj.xml_str()), 'w') as sfile:
            sfile.write(xmlstring)

# Helper method that generates an XML entry in the proper format for _info.xml files.
# @param name: name of the test
# @param type: type of the test (PGA_BLL, etc)
# @param hw: hardware type, but also the xml_str() representation of the project
# @param speed: speed of the test (FIBER10G, etc)
def generate_entry(name, type, hw, speed):
    str = """<script path="./%s.tcl">
    <requiredTags>
        <package value="%s"/>
        <regressionType value="DEV_CI"/>
        <hwType value="%s"/>
    </requiredTags>
    <email value="ENGSTCL2L3@spirent.com"/>

    <parameters>
        <phy value="%s"/>
    </parameters>
</script>\n""" % (name, type, hw, speed)
    return str

# A class for formatting the test inline forms. This is for readonly access.
class ReadOnlyTestInline(admin.TabularInline):
    model = TestCase
    readonly_fields = ('test_name', 'test_group', 'test_folder', 'parent_project', 'owner', 'bugs', 'link')
    exclude = ('test_bugs',)
    template = 'tabtest.html'
    can_delete = False 
    
    # A method that adds a newline separated list of CRs on the instance of the test.
    # @param instance: the instance containing information about the model instance represented by the inline.
    def bugs(self, instance):
        out = ''
        for item in Bug.objects.filter(test_parent=instance):
            if item.bug_status == False:
                out += str(item) + '\n'
        return out
    
    # A method that adds a link in the inline which leads to the corresponding test change page.
    # @param instance: the instance containing information about the model instance represented by the inline.
    def link(self, instance):
        url = '/admin/%s/%s/%s' % (instance._meta.app_label,  instance._meta.module_name, instance.id)
        return mark_safe(u'<a href="{u}">View</a>'.format(u=url))
    
    # A method that takes away add permission from the inline.
    # @param request: the request of the web page
    def has_add_permission(self, request):
        return False

# A class for formatting the bug inline forms. This is for readonly access.
class ReadOnlyBugInline(admin.TabularInline):
    model = TestCase.test_bugs.through
    verbose_name = 'Bug'
    verbose_name_plural = 'Existing Bugs'
    can_delete = False 
    
    readonly_fields = ('bug', 'resolved', 'description', 'tags', 'link',)
    
    # A method that displays whether the inline bug instance is resolved.
    # @param instance: the instance containing information about the model instance represented by the inline.
    def resolved(self, instance):
        return instance.bug.bug_status
    
    # A method that displays the bug description for an element in BugInline.
    # @param instance: the instance containing information about the model instance represented by the inline.
    def description(self, instance):
        return instance.bug.bug_desc
    
    # A method that displays the tags in a comma separated list for a bug.
    # @param instance: the instance containing information about the model instance represented by the inline.
    def tags(self, instance):
        qs = Tag.objects.filter(tag_parent = instance.bug)
        out = ''
        for tag in qs:
            out += str(tag) + ', '
        return out[:-2]
    
    # A method that adds a link in the inline which leads to the corresponding test change page.
    # @param instance: the instance containing information about the model instance represented by the inline.
    def link(self, instance):
        url = '/admin/%s/bug/%s' % (instance._meta.app_label, instance.bug.id)
        return mark_safe(u'<a href="{u}">View</a>'.format(u=url))
    
    # A method that takes away add permission from the inline.
    # @param request: the request of the web page
    def has_add_permission(self, request):
        return False

# A class for formatting the run record inline forms. This is for readonly access.
class ReadOnlyRecordInline(admin.TabularInline):
    model = RunRecord
    template = 'tabtest.html'
    can_delete = False 
    readonly_fields = ('record_group', 'parent_test', 'record_passed', 'record_owner', 'record_log', 'record', 'test')
    
    # A method that adds a link in the inline which leads to the corresponding run record change page.
    # @param instance: the instance containing information about the model instance represented by the inline.
    def record(self, instance):
        url = '/admin/%s/%s/%s' % (instance._meta.app_label,  instance._meta.module_name, instance.id)
        return mark_safe(u'<a href="{u}">View</a>'.format(u=url))
    
    # A method that adds a link in the inline which leads to the corresponding test change page.
    # @param instance: the instance containing information about the model instance represented by the inline.
    def test(self, instance):
        url = '/admin/regression/testcase/' + str(instance.parent_test.id)
        return mark_safe(u'<a href="{u}">View</a>'.format(u=url))
    
    # A method that takes away add permission from the inline.
    # @param request: the request of the web page
    def has_add_permission(self, request):
        return False

# A class for formatting the tag inline forms.
class TagInline(admin.TabularInline):
    
    # A method that overrides the unicode representation of the automatically generated 'through'
    # model to make it more human readable.
    def new_unicode(self):
        return 'Tag Name'
    
    model = Bug.bug_tags.through
    model.__unicode__ = new_unicode
    extra = 0
    verbose_name = 'Tag'
    verbose_name_plural = 'Tags'
    readonly_fields = ('link',)

    # A method that adds a link in the inline which leads to the corresponding tag change page.
    # @param instance: the instance containing information about the model instance represented by the inline.
    def link(self, instance):
        url = '/admin/%s/tag/%s' % (instance._meta.app_label, instance.tag.id)
        return mark_safe(u'<a href="{u}">View</a>'.format(u=url))

# A class for formatting the bug inline forms.
class BugInline(admin.TabularInline):

    # A method that overrides the unicode representation of the automatically generated 'through'
    # model to make it more human readable.
    def new_unicode(self):
        return 'CR Number'
    
    model = TestCase.test_bugs.through
    model.__unicode__ = new_unicode
    extra = 0
    verbose_name = 'Bug'
    verbose_name_plural = 'Existing Bugs'
    readonly_fields = ('resolved', 'description', 'tags', 'link',)
    
    # A method that displays whether the inline bug instance is resolved.
    # @param instance: the instance containing information about the model instance represented by the inline.
    def resolved(self, instance):
        return instance.bug.bug_status
    
    # A method that displays the bug description for an element in BugInline.
    # @param instance: the instance containing information about the model instance represented by the inline.
    def description(self, instance):
        return instance.bug.bug_desc
    
    # A method that displays the tags in a comma separated list for a bug.
    # @param instance: the instance containing information about the model instance represented by the inline.
    def tags(self, instance):
        qs = Tag.objects.filter(tag_parent = instance.bug)
        out = ''
        for tag in qs:
            out += str(tag) + ', '
        return out[:-2]
    
    # A method that adds a link in the inline which leads to the corresponding bug change page.
    # @param instance: the instance containing information about the model instance represented by the inline.
    def link(self, instance):
        url = '/admin/%s/bug/%s' % (instance._meta.app_label, instance.bug.id)
        return mark_safe(u'<a href="{u}">View</a>'.format(u=url))

# A class for formatting the test case inline forms.
class TestInline(admin.TabularInline):
    model = TestCase
    extra = 0
    exclude = ('test_bugs',)
    readonly_fields = ('bugs', 'link',)

    # A method that adds a newline separated list of CRs on the instance of the test.
    # @param instance: the instance containing information about the model instance represented by the inline.
    def bugs(self, instance):
        out = ''
        for item in Bug.objects.filter(test_parent=instance):
            if item.bug_status == False:
                out += str(item) + '\n'
        return out
    
    # A method that adds a link in the inline which leads to the corresponding test case change page.
    # @param instance: the instance containing information about the model instance represented by the inline.
    def link(self, instance):
        url = '/admin/%s/%s/%s' % (instance._meta.app_label,  instance._meta.module_name, instance.id)
        return mark_safe(u'<a href="{u}">View</a>'.format(u=url))

# A class for formatting the run record inline forms.
class RecordInline(admin.TabularInline):
    model = RunRecord
    verbose_name_plural = 'Run Records'
    extra = 0
    readonly_fields = ('link',)
    
    # A method that adds a link in the inline which leads to the corresponding run record change page.
    # @param instance: the instance containing information about the model instance represented by the inline.
    def link(self, instance):
        url = '/admin/%s/%s/%s' % (instance._meta.app_label,  instance._meta.module_name, instance.id)
        return mark_safe(u'<a href="{u}">View</a>'.format(u=url))

# A class that extends the filter functionality. This is used to provide a custom filter on the right hand
# side of the model change selection page. This lets the user filter bugs by project.
class RemainingBugsFilter(SimpleListFilter):
    title = 'Project'
    parameter_name = 'project'

    # Defines the labels to be listed in the filter box. Runs a loop through all projects and adds the project
    # to the labels list.
    # @param request: request sent to the page
    # @param model_admin: unused overridden method for this purpose
    def lookups(self, request, model_admin):
        labels = []
        for project in Project.objects.all():
            labels.append((str(project), str(project)))
        return labels

    # Defines the logic used for filtering the bug instances. This is done in a relatively inefficient way by
    # getting a queryset for all tests in the requested project. For each test, we then filter the queryset of
    # all bugs to get a qset of only the bugs in the test. We then concatenate these qsets together (while
    # excluding duplicate bugs since bug and test are M2M fields) and return the resultinng qset.
    # @param request: request sent to the page
    # @param queryset: the queryset given to the method, in the default case, the qset of all bugs.
    def queryset(self, request, queryset):
        for project in Project.objects.all():
            qset = Bug.objects.none()
            if self.value() == str(project):
                for test in TestCase.objects.filter(parent_project=project):
                    if queryset.filter(test_parent=test).exists():
                        nset = queryset.filter(test_parent=test)
                        if qset.exists():
                            for ex in qset.all():
                                nset = nset.exclude(bug_name=ex.bug_name)
                        qset = qset | nset
                return qset

# A class that extends the filter functionality. This is used to provide a custom filter on the right hand
# side of the model change selection page. This lets the user filter run records by project.
class RecordProjectFilter(SimpleListFilter):
    title = 'Project'
    parameter_name = 'project'

    # Defines the labels to be listed in the filter box. Runs a loop through all projects and adds the project
    # to the labels list.
    # @param request: request sent to the page
    # @param model_admin: unused overridden method for this purpose
    def lookups(self, request, model_admin):
        labels = []
        for project in Project.objects.all():
            labels.append((str(project), str(project)))
        return labels

    # Defines the logic used for filtering the run record instances. This is done ina  relativelty inefficent
    # way similar to RemainingBugFilter. We filter every Test Case in the reuqested project. For each test
    # case we get all of the associated run records from queryset. We concatenate all of these together and
    # return the result.
    # @param request: request sent to the page
    # @param queryset: the queryset given to the method, in the default case, the qset of all run records.
    def queryset(self, request, queryset):
        for project in Project.objects.all():
            qset = RunRecord.objects.none()
            if self.value() == str(project):
                for test in TestCase.objects.filter(parent_project=project):
                    nset = queryset.filter(parent_test=test)
                    qset = qset | nset
                return qset

# A class that manages and defines the logic of the project admin interface. This class includes a custom
# permission system that uses groups. The class defines fieldsets that will be shown on change pages.
class ProjectAdmin(reversion.VersionAdmin):
    fieldsets = [
        ('Project Info', {'fields': ['project_name', 'project_speed']}),
        ('Collaborators', {'fields': ['project_collaborators']}),
        ('Test Sets', {'fields': ['project_test_info']}),
    ]
    filter_horizontal = ('project_collaborators', 'project_test_info')
    list_display = ('__unicode__', 'project_speed', 'get_collaborators', 'get_count')
    
    # A method that is called when the user navigates to the add project page. This makes sure that everything
    # on the add page is editable.
    # @param request: request sent to the page    
    def add_view(self, request):
        self.fieldsets = [
            ('Project Info', {'fields': ['project_name', 'project_speed']}),
            ('Collaborators', {'fields': ['project_collaborators']}),
            ('Test Sets', {'fields': ['project_test_info']}),
        ]
        self.filter_horizontal = ('project_collaborators', 'project_test_info')
        self.inlines = [TestInline]
        self.readonly_fields = ()
        return super(ProjectAdmin, self).add_view(request)

    # A method that is called when the user navigates to the change project page. This makes the entire page
    # read-only if the user is not a manager/superuser and not listed as a collaborator in the project. If
    # the user is a collaborator, then the user will have write acces.
    # @param request: request sent to the page
    # @param object_id: the id of the project being changed
    # @param form_url: the specific form url used by change_view
    # @param extra_context: provides extra context but unused here
    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.fieldsets = [
            ('Project Info', {'fields': ['project_name', 'project_speed']}),
            ('Collaborators', {'fields': ['project_collaborators']}),
        ]
        self.filter_horizontal = ('project_collaborators',)
        self.readonly_fields = ()
        if (request.user.groups.all().filter(name='Manager').exists()):
            self.inlines = [TestInline]
        elif (request.user.groups.all().filter(name='Developer').exists()):
            if (Project.objects.filter(id=object_id)[0].project_collaborators.all().filter(username=request.user).exists()):
                self.inlines = [TestInline]
                self.readonly_fields = ('project_collaborators',)
            else:
                self.inlines = [ReadOnlyTestInline]
                self.readonly_fields = ('project_name', 'project_collaborators', 'project_speed')
        return super(ProjectAdmin, self).change_view(request, object_id, form_url, extra_context=extra_context)

    # A helper method that gets a list of tests by using the collectonly parameter of pytest. This takes an
    # argument that specifies what type of test to get (PGA_BLL, RFC_BENCHMARK_BLL). The method uses regular
    # expressions to parse the pytest output and creates the tests in the database with the proper parent
    # project, user, name, folder, and group.
    # @param type: the type of test (PGA_BLL, etc)
    # @param request: request sent to the page
    # @param obj: the project that the tess will be added to4
    def get_tests(self, type, request, obj):
        hwtypes = ''
        commands = ['python', 'run.py', 
                    "--collectonly", 
                    "-m '(%s) and not slow and not disabled'" % (type)]
        if TestInfo.objects.filter(info_name='ALL')[0] not in obj.project_test_info.all():
            for item in obj.project_test_info.all():
                hwtypes += 'HW_' + str(item) + ' or '
            if hwtypes != '':
                hwtypes = hwtypes[:-4]
                commands = ['python', 'run.py', 
                            "--collectonly", 
                            "-m '(%s) and (%s) and not slow and not disabled'" % (type, hwtypes)]
            else:
                return
        output = Popen(commands, stdout=PIPE, cwd=settings.REG_PATH)
        comlist =  output.stdout.read().split('\n')
        testset = set()
        folder = ''
        for file in comlist:
            matchobj = re.search("<TclItem '(.)+'>", file)
            if matchobj:
                trunc = matchobj.group()[10:-2]
                n = len(trunc) - 1
                while n > 0:
                    if trunc[n] == '_':
                        trunc = trunc[:n]
                        break
                    n -= 1
                if (trunc in testset) == False:
                    testset.add(trunc)
                    trunc = TestCase(parent_project=obj, test_name = trunc, owner = request.user, test_group = type, test_folder = folder)
                    trunc.save()
            else:
                matchobj = re.search("<TclFile '(.)+'>", file)
                if matchobj:
                    trunc = matchobj.group()[10:-2]
                    index = trunc.find('/')
                    if index != -1:
                        trunc = trunc[:index]
                        folder = trunc

    # A method that overrides the default save functionality. If the object was just created, add
    # default blank values to the regression info ports and automatically add the project_owner.
    # Also automatically populate tests with all tests in the regression folder pertaining to PGA_BLL, 
    # CAPTURE_BLL, LEARNING_BLL, and RFC_BENCHMARKING_BLL.
    # @param request: request sent to the page
    # @param obj: the project being added/updated
    # @param form: the form used for editing the project
    # @param change: a boolean specifying whether the user is adding or changing a project
    def save_model(self, request, obj, form, change):
        if change == False:
            obj.project_owner = request.user
            reginfo = RegressionInfo(regression_portone='', regression_porttwo='',
                                     regression_portthree='', regression_portfour='',
                                     regression_portfive='', regression_portsix='')
            reginfo.save()
            obj.project_regression_info = reginfo
            obj.save()
        obj.save()

    # A method that is called when saving related fields, primarily M2M fields. This is similar to save_model,
    # but also required because ManyToMany fields are not updated under save_model. The primary purpose of
    # this method is to automatically add managers to the M2M collaborators field of each project.
    # @param request: request sent to the page
    # @param form: the form used for editing the project
    # @param formset: unused for this function override
    # @param change: a boolean specifying whether the user is adding or changing a project
    def save_related(self, request, form, formsets, change):
        super(ProjectAdmin, self).save_related(request, form, formsets, change)
        proj = form.save(commit=False)
        if change == False:
            self.get_tests('PGA_BLL', request, proj)
            self.get_tests('CAPTURE_BLL', request, proj)
            self.get_tests('LEARNING_BLL', request, proj)
            self.get_tests('RFC_BENCHMARKING_BLL', request, proj)
            proj.save()
        for user in User.objects.filter(groups__name='Manager'):
            proj.project_collaborators.add(user)
        create_xml(proj)
    
    # A method that saves the formset. Not currently used at all in the code, but perhaps in the future.
    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for instance in instances:
            instance.save()
            
# A class that manages and defines the logic of the test case interface. This class includes a custom
# permission system that uses groups. The class defines fieldsets that will be shown on change pages, 
# search_fields on the select change page, and list_filters on the select change page. 
class TestCaseAdmin(reversion.VersionAdmin):
    fieldsets = [
        (None, {'fields': ['test_name', 'test_folder']}),
        ('Project Information', {'fields': ('test_group', 'parent_project', 'owner')}),
    ]
    search_fields = ['test_name']
    change_list_template = 'test_change_list.html'
    list_display = ('colored_name', 'owner', 'parent_project', 'get_bugs', 'failed_runs')
    list_filter = ['parent_project', 'owner', 'create_date']
    inlines = [BugInline, RecordInline]
    # This function is called when a user navigates to the test case add page. The code in here ensures that
    # all fields have write access.
    # @param request: request sent to the page
    def add_view(self, request):
        self.readonly_fields = ()
        return super(TestCaseAdmin, self).add_view(request)
    
    # This function is called when a user navigates to the change test case page. This defines a permission
    # system based on Manager and Developer groups. If the user requesting access is not a collaborator on
    # the parent project of the test, then only give read-only access, while if not, then give full access.
    # @param request: request sent to the page
    # @param object_id: the id of the test case being changed
    # @param form_url: the specific form url used by change_view
    # @param extra_context: provides extra context but unused here
    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.readonly_fields = ('test_group',)
        if (request.user.groups.all().filter(name='Manager').exists()):
            self.inlines = [BugInline, RecordInline]
        elif (request.user.groups.all().filter(name='Developer').exists()):
            res = TestCase.objects.filter(id=object_id)[0].parent_project
            if (res.project_collaborators.all().filter(username=request.user).exists()):
                self.inlines = [BugInline, RecordInline]
            else:
                self.inlines = [ReadOnlyBugInline, ReadOnlyRecordInline]
                self.readonly_fields = ('test_folder', 'test_group', 'test_name', 'parent_project', 'owner')
        return super(TestCaseAdmin, self).change_view(request, object_id, form_url, extra_context=extra_context)
    
    # Depricated method used while developing. Left in for convenience of reverting back.
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        print db_field
        if db_field.name == 'parent_project':
            kwargs["queryset"] = Project.objects.filter(project_collaborators=request.user)
        return super(TestCaseAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
    
    # A method that saves the formset. Not currently used at all in the code, but perhaps in the future.
    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for instance in instances:
            instance.save()

# A class that manages and defines the logic of the bug interface. Unlike the TestCaseAdmin and the
# ProjectAdmin, this class does not have a permissions system since everyone should be able to maintain
# bugs in any project. The class defines fieldsets that will be shown on change pages, search_fields on the 
# select change page, and list_filters on the select change page. 
class BugAdmin(reversion.VersionAdmin):
    list_filter = ['bug_status', RemainingBugsFilter, 'bug_tags']
    fieldsets = [
        ('Bug Name', {'fields': ['bug_name']}),
        ('Bug Descritpion', {'fields': ['bug_desc']}),
        ('Bug Status', {'fields': ['bug_status']})
    ]
    inlines = [TagInline]
    filter_horizontal = ('bug_tags',)
    search_fields = ['bug_desc', 'bug_name', 'bug_tags__tag_name']
    list_display = ('__unicode__', 'get_desc', 'bug_status', 'get_parents')
    
    # Override the save model functionality. Automatically sets the owner of the bug to the person who created
    # the bug.
    # @param request: request sent to the page
    # @param obj: the bug being added/updated
    # @param form: the form used for editing the bug
    # @param change: a boolean specifying whether the user is adding or changing a bug
    def save_model(self, request, obj, form, change):
        obj.bug_owner = request.user
        obj.save()

# A class that manages and defines the logic of the run record interface. This has a group based permission
# system. If the user is a manager or on the project, then the user gets write access, else read-only. This
# class doesn't have a special add_view override because the user shouldn't be manually adding run records.
# Run records are automatically added by the regression utility. The class defines fieldsets that will be 
# shown on change pages and list_filters on the select change page. 
class RecordAdmin(reversion.VersionAdmin):
    list_filter = ['record_passed', RecordProjectFilter]
    fieldsets = [
        ('Record Info', {'fields': ['parent_test', 'record_group', 'record_owner']}),
        ('Run Info', {'fields': ['record_log', 'record_passed']})
    ]
    list_display = ('__unicode__', 'parent_test', 'record_owner', 'record_passed')
    
    # This function is called when a user navigates to the change test case page. This defines a permission
    # system based on Manager and Developer groups. If the user requesting access is not a collaborator on
    # the parent project of the test with this run record, then only give read-only access, while if not, 
    # then give full access.
    # @param request: request sent to the page
    # @param object_id: the id of the run record being changed
    # @param form_url: the specific form url used by change_view
    # @param extra_context: provides extra context but unused here
    def change_view(self, request, object_id, form_url='', extra_context=None):
        if (request.user.groups.all().filter(name='Manager').exists()):
            self.readonly_fields = ()
        elif (request.user.groups.all().filter(name='Developer').exists()):
            res = (RunRecord.objects.filter(id=object_id)[0].parent_test).parent_project
            if (res.project_collaborators.all().filter(username=request.user).exists()):
                self.readonly_fields = ()
            else:
                self.readonly_fields = ('record_owner', 'record_log', 'parent_test', 'record_passed', 'record_group')
        return super(RecordAdmin, self).change_view(request, object_id, form_url, extra_context=extra_context)
    
    # Override the save model functionality. Automatically sets the owner of the run record to the person
    # who created the run record
    # @param request: request sent to the page
    # @param obj: the run record being added/updated
    # @param form: the form used for editing the run record
    # @param change: a boolean specifying whether the user is adding or changing a run record
    def save_model(self, request, obj, form, change):
        obj.project_owner = request.user
        obj.save()

# A class that manages and defines the logic of the tag interface. There is no permissions system. It 
# specifies a fieldset.
class TagAdmin(reversion.VersionAdmin):
    fieldsets = [
        ('Tag Name', {'fields': ['tag_name']}),
    ]

# A class that manages and defines the logic of the test info interface. There is no permissions system. It 
# specifies a fieldset. Not currently used.
class TestInfoAdmin(reversion.VersionAdmin):
    fieldsets = [
        ('Test Info Name', {'fields': ['info_name']}),
    ]

# A class that manages and defines the logic of the regression group interface. There is no permissions
# system, but it is read-only for every user for viewing. This is usually not created by a user because it
# is the job of the regression utility to create and populate these.
class GroupAdmin(reversion.VersionAdmin):
    list_filter = ['reg_group_project',]
    fieldsets = [
        ('Regression Info', {'fields': ['reg_group_project', 'reg_group_start', 'reg_group_end',
                                        'reg_group_bll', 'reg_group_il', 'reg_group_runner', 'reg_group_file']}),
        ('Regression Log', {'fields': ['reg_group_log']})
    ]
    inlines = [RecordInline]
    readonly_fields = ()
    
    # Makes the form write-able in the odd case where the user will need to manually create a group.
    # @param request: request sent to the page
    def add_view(self, request):
        self.inlines = [RecordInline]
        self.readonly_fields = ()
        return super(GroupAdmin, self).add_view(request)
    
    # This function is called when a user navigates to the change group page. This is intended as more of a
    # viewing page than a change page. This sets everything to read only so the user can get information about
    # the regression run.
    # @param request: request sent to the page
    # @param object_id: the id of the regression group being changed
    # @param form_url: the specific form url used by change_view
    # @param extra_context: provides extra context but unused here
    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.readonly_fields = ('reg_group_project', 'reg_group_start', 'reg_group_end', 
                           'reg_group_bll', 'reg_group_il', 'reg_group_runner')
        self.inlines = [ReadOnlyRecordInline]
        return super(GroupAdmin, self).change_view(request, object_id, form_url, extra_context=extra_context)
    list_display = ('__unicode__', 'reg_group_project', 'reg_group_runner', 'get_passed')

admin.site.register(Project, ProjectAdmin)
admin.site.register(TestCase, TestCaseAdmin)
admin.site.register(Bug, BugAdmin)
admin.site.register(RunRecord, RecordAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(TestInfo, TestInfoAdmin)
admin.site.register(RegressionGroup, GroupAdmin)