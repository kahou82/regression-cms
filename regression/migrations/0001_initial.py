# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Project'
        db.create_table(u'regression_project', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('modified_date', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('project_name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('project_owner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('project_regression_info', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['regression.RegressionInfo'])),
            ('project_speed', self.gf('django.db.models.fields.CharField')(default='FIBER10G', max_length=64)),
        ))
        db.send_create_signal(u'regression', ['Project'])

        # Adding M2M table for field project_collaborators on 'Project'
        m2m_table_name = db.shorten_name(u'regression_project_project_collaborators')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('project', models.ForeignKey(orm[u'regression.project'], null=False)),
            ('user', models.ForeignKey(orm[u'auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['project_id', 'user_id'])

        # Adding M2M table for field project_test_info on 'Project'
        m2m_table_name = db.shorten_name(u'regression_project_project_test_info')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('project', models.ForeignKey(orm[u'regression.project'], null=False)),
            ('testinfo', models.ForeignKey(orm[u'regression.testinfo'], null=False))
        ))
        db.create_unique(m2m_table_name, ['project_id', 'testinfo_id'])

        # Adding model 'TestCase'
        db.create_table(u'regression_testcase', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('modified_date', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('parent_project', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['regression.Project'])),
            ('test_name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('test_group', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('test_folder', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('owner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
        ))
        db.send_create_signal(u'regression', ['TestCase'])

        # Adding M2M table for field test_bugs on 'TestCase'
        m2m_table_name = db.shorten_name(u'regression_testcase_test_bugs')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('testcase', models.ForeignKey(orm[u'regression.testcase'], null=False)),
            ('bug', models.ForeignKey(orm[u'regression.bug'], null=False))
        ))
        db.create_unique(m2m_table_name, ['testcase_id', 'bug_id'])

        # Adding model 'RunRecord'
        db.create_table(u'regression_runrecord', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('modified_date', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('parent_test', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['regression.TestCase'])),
            ('record_group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['regression.RegressionGroup'])),
            ('record_owner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('record_log', self.gf('django.db.models.fields.TextField')()),
            ('record_passed', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'regression', ['RunRecord'])

        # Adding model 'Bug'
        db.create_table(u'regression_bug', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('modified_date', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('bug_name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('bug_status', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('bug_owner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('bug_desc', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'regression', ['Bug'])

        # Adding M2M table for field bug_tags on 'Bug'
        m2m_table_name = db.shorten_name(u'regression_bug_bug_tags')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('bug', models.ForeignKey(orm[u'regression.bug'], null=False)),
            ('tag', models.ForeignKey(orm[u'regression.tag'], null=False))
        ))
        db.create_unique(m2m_table_name, ['bug_id', 'tag_id'])

        # Adding model 'Tag'
        db.create_table(u'regression_tag', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tag_name', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'regression', ['Tag'])

        # Adding model 'TestInfo'
        db.create_table(u'regression_testinfo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('info_name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('info_software_project', self.gf('django.db.models.fields.CharField')(default='', max_length=200)),
        ))
        db.send_create_signal(u'regression', ['TestInfo'])

        # Adding model 'RegressionInfo'
        db.create_table(u'regression_regressioninfo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('regression_portone', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('regression_porttwo', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('regression_portthree', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('regression_portfour', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('regression_portfive', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('regression_portsix', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('regression_portseven', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('regression_porteight', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('regression_portnine', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('regression_portten', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal(u'regression', ['RegressionInfo'])

        # Adding model 'RegressionGroup'
        db.create_table(u'regression_regressiongroup', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('reg_group_project', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['regression.Project'])),
            ('reg_group_start', self.gf('django.db.models.fields.DateTimeField')()),
            ('reg_group_end', self.gf('django.db.models.fields.DateTimeField')()),
            ('reg_group_bll', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('reg_group_il', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('reg_group_runner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('reg_group_log', self.gf('django.db.models.fields.TextField')()),
            ('reg_group_file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal(u'regression', ['RegressionGroup'])


    def backwards(self, orm):
        # Deleting model 'Project'
        db.delete_table(u'regression_project')

        # Removing M2M table for field project_collaborators on 'Project'
        db.delete_table(db.shorten_name(u'regression_project_project_collaborators'))

        # Removing M2M table for field project_test_info on 'Project'
        db.delete_table(db.shorten_name(u'regression_project_project_test_info'))

        # Deleting model 'TestCase'
        db.delete_table(u'regression_testcase')

        # Removing M2M table for field test_bugs on 'TestCase'
        db.delete_table(db.shorten_name(u'regression_testcase_test_bugs'))

        # Deleting model 'RunRecord'
        db.delete_table(u'regression_runrecord')

        # Deleting model 'Bug'
        db.delete_table(u'regression_bug')

        # Removing M2M table for field bug_tags on 'Bug'
        db.delete_table(db.shorten_name(u'regression_bug_bug_tags'))

        # Deleting model 'Tag'
        db.delete_table(u'regression_tag')

        # Deleting model 'TestInfo'
        db.delete_table(u'regression_testinfo')

        # Deleting model 'RegressionInfo'
        db.delete_table(u'regression_regressioninfo')

        # Deleting model 'RegressionGroup'
        db.delete_table(u'regression_regressiongroup')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'regression.bug': {
            'Meta': {'object_name': 'Bug'},
            'bug_desc': ('django.db.models.fields.TextField', [], {}),
            'bug_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'bug_owner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'bug_status': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'bug_tags': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'tag_parent'", 'symmetrical': 'False', 'to': u"orm['regression.Tag']"}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'regression.project': {
            'Meta': {'object_name': 'Project'},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'project_collaborators': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'user_project'", 'symmetrical': 'False', 'to': u"orm['auth.User']"}),
            'project_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'project_owner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'project_regression_info': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['regression.RegressionInfo']"}),
            'project_speed': ('django.db.models.fields.CharField', [], {'default': "'FIBER10G'", 'max_length': '64'}),
            'project_test_info': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'info_project'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['regression.TestInfo']"})
        },
        u'regression.regressiongroup': {
            'Meta': {'object_name': 'RegressionGroup'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reg_group_bll': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'reg_group_end': ('django.db.models.fields.DateTimeField', [], {}),
            'reg_group_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'reg_group_il': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'reg_group_log': ('django.db.models.fields.TextField', [], {}),
            'reg_group_project': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['regression.Project']"}),
            'reg_group_runner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'reg_group_start': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'regression.regressioninfo': {
            'Meta': {'object_name': 'RegressionInfo'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'regression_porteight': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'regression_portfive': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'regression_portfour': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'regression_portnine': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'regression_portone': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'regression_portseven': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'regression_portsix': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'regression_portten': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'regression_portthree': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'regression_porttwo': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'regression.runrecord': {
            'Meta': {'object_name': 'RunRecord'},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'parent_test': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['regression.TestCase']"}),
            'record_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['regression.RegressionGroup']"}),
            'record_log': ('django.db.models.fields.TextField', [], {}),
            'record_owner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'record_passed': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'regression.tag': {
            'Meta': {'object_name': 'Tag'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tag_name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'regression.testcase': {
            'Meta': {'object_name': 'TestCase'},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'parent_project': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['regression.Project']"}),
            'test_bugs': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'test_parent'", 'symmetrical': 'False', 'to': u"orm['regression.Bug']"}),
            'test_folder': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'test_group': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'test_name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'regression.testinfo': {
            'Meta': {'object_name': 'TestInfo'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'info_software_project': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200'})
        }
    }

    complete_apps = ['regression']