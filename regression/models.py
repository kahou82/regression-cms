# This file defines all of the database models in the PGA CMS utility. The majority of these modles are
# editable in the Django admin. Note that many of these models inherit from TimestampModel, which has a
# 'create_date' field that autostamps the creation of each model with a time and date.
# @author Brian Ho
# @date 7/22/13
# @email bho6@jhu.edu

from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from framework.models import TimestampModel
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.conf import settings
import os 

# A class that defines what a project is in terms of the regression utility. Sample projects are 'Warpath 10G'
# and 'Wraith 40G'.
class Project(TimestampModel):
    
    # The name of the project. (Warpath 10G)
    project_name = models.CharField(max_length=200)
    
    # The User who owns the project, almost always a manager.
    project_owner = models.ForeignKey(User)
    
    # A group of Users that are working on the project. Only Users specified in this field have write access
    # to the project and its associated TestCases and RunRecords.
    project_collaborators = models.ManyToManyField(User, related_name='user_project')
    
    # A group of tests that is only assigned to once. This is currently not being used, but will probably be
    # in the future.
    project_test_info = models.ManyToManyField('TestInfo', related_name='info_project', null=True, blank=True)
    
    # The regression info for the project that contains 3 pairs of ports that the regression will run on.
    project_regression_info = models.ForeignKey('RegressionInfo')
    
    # The speed that the project will be run on. (Ex: FIBER10G, FIBER40G, FIBER100G)
    project_speed = models.CharField(max_length=64, default='FIBER10G')
    
    # The unicode/str method that returns the project name.
    # @return: the project name
    def __unicode__(self):
        return self.project_name
    
    # This method is used for list_diplay in the select change page. It gets a string representation of the
    # list of Users in the project_collaborator field.
    # @return: a comma separated string of Users
    def get_collaborators(self):
        s = ""
        for collab in self.project_collaborators.all():
            s += str(collab) + ', '
        return s[:-2]
    
    # A method that gets the number of TestCases that have parent_project set to this project.
    # @return: the number of test cases
    def get_count(self):
        return TestCase.objects.filter(parent_project=self).count()
    
    # A method used in saving and getting _info.xml files. This takes the project name and makes it machine
    # friendly when saving as a filename. It removes all spaces in the name and makes everything uppercase.
    # @return: a machine friendly string
    def xml_str(self):
        s = self.project_name
        s = s.replace(" ", "")
        return s.upper() + 'AUTO'
    get_count.short_description = 'Tests Count'
    get_collaborators.short_description = 'Collaborators'

# A class that defines what a test case is. This is usually a .tcl file such as 'CaptureBufferActionTest'
# or 'CaptureOffsetFilterMacAddress'. A test case is considered passing if there are no unresolved bugs
# associated with it.
class TestCase(TimestampModel):
    
    # A Project that the TestCase is part of.
    parent_project = models.ForeignKey(Project)
    
    # A list of resolved and unresolved bugs that are associated with the test.
    test_bugs = models.ManyToManyField('Bug', related_name='test_parent')
    
    # A name of the test that corresponds to the name of the .tcl file in the regression folder.
    test_name = models.CharField(max_length=200)
    
    # The group of the test such as PGA_BLL and RFC_BENCHMARKING_BLL that corresponds to a type in the
    # .xml files.
    test_group = models.CharField(max_length=64)
    
    # The subfolder under /regression/ where the .tcl file resides.
    test_folder = models.CharField(max_length=64)
    
    # The owner of the test and the person in charge of running that test.
    owner = models.ForeignKey(User)
    
    # A string representation of a Test Case.
    # @return: the test name from test_name
    def __unicode__(self):
        return self.test_name
    
    # A method that gets the number of unresolved bugs in a test case. If this number is zero, then the
    # test is considered passing.
    # @return: the number of bugs in the script.
    def get_bugs(self):
        count = 0
        for item in self.test_bugs.all():
            if item.bug_status == False:
                count += 1
        return count
    
    # A method that gets the number of failed runs in a test case.
    # @return: the number of failed runs in a test case.
    def failed_runs(self):
        count = 0
        for record in RunRecord.objects.filter(parent_test = self):
            if record.record_passed == False:
                count += 1
        return count
    
    # A method used for the select change view that provides coloring HTML for each listing. If the color is
    # green, then the test is passing. If the color is reg, then the test is failing.
    # @return: HTML for colored text.
    def colored_name(self):
        if self.get_bugs() == 0:
            return '<span style="color: #008000;">%s</span>' % (self.test_name)
        else:
            return '<span style="color: #FF0000;">%s</span>' % (self.test_name)
    colored_name.allow_tags = True
    colored_name.short_description = 'Test Name'
    get_bugs.short_description = 'Unresolved CRs'

# A class that defines a run of a test case. This model is usually automatically generated by the regression
# utility.
class RunRecord(TimestampModel):
    
    # The test case that the run record was run under.
    parent_test = models.ForeignKey(TestCase)
    
    # The regression batch/group that the test was run with.
    record_group = models.ForeignKey('RegressionGroup')
    
    # The User who ran the run record.
    record_owner = models.ForeignKey(User)
    
    # The log generated by pytest.
    record_log = models.TextField()
    
    # A boolean that tells whether the test case passed or failed under that specific run record.
    record_passed = models.BooleanField('Passed')
    
    # A string representation of a run record. Instead of a name, this returns a formatted string with time
    # and date that the record was run.
    # @return: the string with time and date of create_date
    def __unicode__(self):
        return str(timezone.localtime(self.create_date).strftime("%Y/%m/%d %H:%M"))

# A class that defines what a bug is. This model should correspond to indivdual CRs on the official bug
# reporting utility used by the company.
class Bug(TimestampModel):
    
    # A string that represents the bug, usually the CR number.
    bug_name = models.CharField(max_length=200)
    
    # A boolean indicating whether the bug has been resolved. The application does not delete bugs; instead
    # it marks them as resolved to make going back easier.
    bug_status = models.BooleanField('Resolved', default=False)
    
    # The User who reported the bug.
    bug_owner = models.ForeignKey(User)
    
    # A description that tells what the bug is and might give additional information about the bug.
    bug_desc = models.TextField('Bug Description')
    
    # A list of tags to make searching for bugs easier.
    bug_tags = models.ManyToManyField('Tag', related_name='tag_parent')
    
    # This method returns a list of test cases separated by newlines to be used in the bug select change page.
    # @return: a string of test cases that have the bug
    def get_parents(self):
        s = ""
        for parent in self.test_parent.all():
            s += str(parent) + '<br>'
        return s[:-4]
    
    # The method for returning a string representation of the bug. This merely prepends 'CR#: ' to
    # the bug_name.
    # @return: the string representation
    def __unicode__(self):
        return 'CR#: %s' % (self.bug_name)
    
    # Gets a description of the bug, but truncates it if it is too long since this method will be used when
    # space and layout are issues to consider.
    # @return: a truncated string of the bug_desc
    def get_desc(self):
        info = (self.bug_desc[:100] + '...') if len(self.bug_desc) > 75 else self.bug_desc
        return info
    get_parents.short_description = "Associated Tests"
    get_parents.allow_tags = True
    get_desc.short_description = "Bug Description"

# A class that is really just a string, but used as a tag for Bugs to make searching for Bugs easier.
class Tag(models.Model):
    
    # A string that is the name and the only content of the tag.
    tag_name = models.CharField(max_length=200)
    
    # Returns the name of the tag.
    # @return:  the name of the tag
    def __unicode__(self):
        return self.tag_name

# A class that currently isn't being used.
class TestInfo(models.Model):
    info_name = models.CharField('Test Info Name', max_length=200)
    info_software_project = models.CharField('Test Info Project', max_length=200, default = "")
    def __unicode__(self):
        return self.info_name

# A class that stores three port pairs used for automatically running regressions on the server. This is
# editable via the dashboard and cannot/should not be changed through conventional admin means.
class RegressionInfo(models.Model):
    
    # The first port of the first pair.
    regression_portone = models.CharField(max_length=32)
    
    # The second port of the first pair.
    regression_porttwo = models.CharField(max_length=32)
    
    # The first port of the second pair.
    regression_portthree = models.CharField(max_length=32)
    
    # The second port of the second pair.
    regression_portfour = models.CharField(max_length=32)
    
    # The first port of the third pair.
    regression_portfive = models.CharField(max_length=32)
    
    # The second port of the third pair.
    regression_portsix = models.CharField(max_length=32)

    # The first port of the fourth pair.
    regression_portseven = models.CharField(max_length=32)
    
    # The second port of the fourth pair.
    regression_porteight = models.CharField(max_length=32)

    # The first port of the fifth pair.
    regression_portnine = models.CharField(max_length=32)
    
    # The second port of the fifth pair.
    regression_portten = models.CharField(max_length=32)
    
# A class that represents a group of regression tests run at the same time. This is automatically created
# when a user clicks on Run Regression on the dashboard. This is populated with similarly automatically
# populated RunRecords and is readonly to all Users.
class RegressionGroup(models.Model):
    
    # The project that this was run for.
    reg_group_project =  models.ForeignKey(Project, verbose_name='Project')
    
    # The time the regression run was started.
    reg_group_start = models.DateTimeField('Start Time')
    
    # The time the regression run completed.
    reg_group_end = models.DateTimeField('End Time')
    
    # The build version of the BLL that was used in running regressions.
    reg_group_bll = models.CharField('BLL Build', max_length=64)
    
    # The build version of the IL that was used in running regressions.
    reg_group_il = models.CharField('IL Build', max_length=64)
    
    # The User who executed the regressions.
    reg_group_runner = models.ForeignKey(User, verbose_name='Executed By')
    
    # The log output by pytest.
    reg_group_log = models.TextField()
    
    # A .zip file containing detailed information about the regression run that can be downloaded by a User.
    reg_group_file = models.FileField(upload_to='logs')
    
    # A method that gets the number of psasing tests and outputs this data in the form 'passing/total'.
    # @return: a string representation of the passing vs total tests in a particular regression run.
    def get_passed(self):
        count = 0
        recordset = RunRecord.objects.filter(record_group = self)
        for record in recordset:
            if record.record_passed == True:
                count += 1
        return '%s/%s' % (count, recordset.count())
    
    # Gets a string representation of a run records using the regression start time.
    # @return: a formatted string of a datetime. 
    def __unicode__(self):
        return timezone.localtime(self.reg_group_start).strftime("%Y/%m/%d %H:%M")
    
    # Gets a filename-friendly name for the regression group using a formatted string of a datertime with
    # spaces replaced with dashes.
    # @requires: a machine-friendly string representing the regression run group.
    def file_name(self):
        return timezone.localtime(self.reg_group_start).strftime("%Y-%m-%d-%H-%M").replace(" ", "")
    
    get_passed.short_description = "Passed Tests"

# A method that is triggered when a project is deleted. When a project is deleted, this method is called to
# delete all of the existing _info.xml files for that project.
# @param sender: the instance that sent this request.
# @param **kwargs: a group of arguments that contains, among other things, the instance data of the project
# being deleted.
@receiver(post_delete, sender=Project)
def post_delete_handler(sender, **kwargs):
    proj = kwargs['instance']
    for root, dirs, files in os.walk(settings.REG_PATH):
        for file in files:
            if str(file) == proj.xml_str() + '_info.xml':
                p = os.path.join(root, file)
                os.remove(os.path.abspath(p))