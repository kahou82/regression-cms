from django.conf import settings
from django.core.management import call_command
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "pgacms.settings")

apps = ['framework','regression', 'register']
for app in apps:
    call_command('schemamigration', app, auto=True)

call_command('syncdb', interactive=False)