# This file defines the form for use in upload_module.html.
# @author Brian Ho
# @date 7/22/13
# @email bho6@jhu.edu

from django import forms
from upload.models import TCInfo

# A class that inherits from ModelForm and uses TCInfo as the model. The only field in this form will be the
# tcinfo_file field.
class UploadForm(forms.ModelForm):
    
    # Defines the model and the only field to be displayed.
    class Meta:
        model = TCInfo
        fields = ['tcinfo_file']