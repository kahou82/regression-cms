# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'TCInfo'
        db.create_table(u'upload_tcinfo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('modified_date', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('tcinfo_bll', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('tcinfo_uploader', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('tcinfo_file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal(u'upload', ['TCInfo'])


    def backwards(self, orm):
        # Deleting model 'TCInfo'
        db.delete_table(u'upload_tcinfo')


    models = {
        u'upload.tcinfo': {
            'Meta': {'object_name': 'TCInfo'},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'tcinfo_bll': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'tcinfo_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'tcinfo_uploader': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        }
    }

    complete_apps = ['upload']