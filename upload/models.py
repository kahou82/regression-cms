# This file contains the data models for the upload application. The only model here is TCInfo, the mdoel that
# contains information about the TestCenter BLL version.
# @author Brian Ho
# @date 7/22/13
# @email bho6@jhu.edu

from django.db import models
from framework.models import TimestampModel

# A class that contains info about the TestCenter application. This is updated by uploading a file to the
# UploadModule on the dashboard.
class TCInfo(TimestampModel):
    
    # A string containing the version of the BLL. This defaults to none if a proper version cannot be found.
    # Updated automatically by running a tcl script.
    tcinfo_bll = models.CharField('BLL Build', max_length=256)
    
    # A string representing the username of the person who uploaded the TC BLL version.
    tcinfo_uploader = models.CharField('BLL Uploader', max_length=256)
    
    # A file that is uploaded to the server containing the TestCenter build. This needs to be a zip and will
    # be unzipped to the specified folder in settings.py.
    tcinfo_file = models.FileField('Upload a new build', upload_to='bll')