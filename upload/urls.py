from django.conf.urls import patterns, url
from upload import views

urlpatterns = patterns('',
    url(r'$', views.UploadView.as_view(), name="crajax"),
)