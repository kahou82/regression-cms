# This file concerns itself with the view for uploading files. This view is never displayed, and is only used
# as an intermediate view which redirects back to the admin home page. However, this view is necessary because
# it handles the logic of uploading, unzipping, and updating the versions of TestCenter from the UploadModule.
# @author Brian Ho
# @date 7/22/13
# @email bho6@jhu.edu

from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic.edit import CreateView
from django.conf import settings
from forms import UploadForm
from upload.models import TCInfo
from subprocess import call
from subprocess import Popen, PIPE
import zipfile
import os

# The view that is called when user navigates to /upload/. This performs some logic and redirects the user
# back to the admin home page. This overrides CreateView to make creation of new objects easier.
class UploadView(CreateView):
    form_class = UploadForm
    success_url ='/admin/'
    
    # This function is called when the page is requested with GET. This should not happen unless the user
    # accidentally manually goes to the /upload/ address. This will simply redirect to the main admin page.
    # @param request: request sent to the page
    # @param *args: additional arguments.
    # @param **kwargs: additional arguments.
    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect('/admin/')
    
    # This function is called when the page is requested with POST. This will happen when the user uses the
    # upload module on the dashboard. It will proceed to delete everything in the settings specified
    # TestCenter path and then extract the uploaded .zip file into that path. It also calls the super
    # post method to create the new TCInfo model and delete the old one. This is a little clunky for simply
    # updating a model, but deleting and recreating was a lot easier to program with relatively small loss in
    # efficiency. If an invalid zip file is uploaded, the BLL Version will default to 'None'. Sets the user
    # which is passed through the request post.
    # @param request: request sent to the page
    # @param *args: additional arguments.
    # @param **kwargs: additional arguments.
    def post(self, request, *args, **kwargs):
        if 'tcinfo_file' in request.POST:
            return HttpResponseRedirect('/admin/')
        for info in TCInfo.objects.all():
            info.tcinfo_file.delete()
            info.delete()
        
        response = super(UploadView, self).post(request, *args, **kwargs)
        for root, dirs, files in os.walk(settings.TC_PATH, topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
            for name in dirs:
                os.rmdir(os.path.join(root, name))
        
        tci = TCInfo.objects.all()[0]
        try: 
            with zipfile.ZipFile(settings.MEDIA_ROOT + '/' + tci.tcinfo_file.name, "r") as z:
                z.extractall(settings.TC_PATH)
                commands = ['tclsh', 'version.tcl']
                proc = Popen(commands, stdout=PIPE, cwd=os.path.dirname(os.path.realpath(__file__)))
                output =  proc.stdout.read().split('\n')[0]
                tci.tcinfo_bll = output;
        except Exception, e:
            tci.icinfo_bll = 'None'
        tci.tcinfo_uploader = request.POST['username']
        tci.save()
        return response
